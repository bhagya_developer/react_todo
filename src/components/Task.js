import Button from "./Button"

const Task = ({task, onDelete}) => {
    return(
        <li className='todo-stats'>{task.text}
        <button type="button" className="b1" aria-label="Close">
  <span aria-hidden="true" onClick={() => onDelete(task.id)}y>&times;</span>
</button>
        {/* <Button name="Delete" onClick={() => onDelete(task.id)} /> */}
        </li>

    );

}
export default Task;