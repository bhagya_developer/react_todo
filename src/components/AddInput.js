import { useState } from 'react';
function AddInput({ onAdd }) {
    const [text, setText] = useState('');
    const onSubmit = (e) => {
        e.preventDefault();
        if (!text) {
            alert("Please Add a Task");
            return;
        }
        onAdd({ text });
        setText('');

    }
    return (
        <p>
            <form className="add-form" onSubmit={onSubmit}>
                    <input
                        type="text"
                        placeholder="Add your new task"
                        value={text}
                        onChange={(e) => setText(e.target.value)} />
                    <input type="submit" className='submit-button' value="Add" />
            </form>
        </p>

    );
}
export default AddInput;