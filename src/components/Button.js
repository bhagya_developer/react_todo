import propTypes from 'prop-types';
const Button = (props) => {
   
    return(
        <button onClick={props.onClick}>{props.name}</button>

    );
}
Button.protoTypes = {
    onClick:propTypes.func
}
export default Button;