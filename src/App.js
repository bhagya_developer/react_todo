import Tasks from './components/Tasks';
import AddInput from './components/AddInput';
import {useState} from 'react';
import './App.css';

function App() {
  const [tasks, setTasks] =useState('')
const addTask = (task) => {
  const id = Math.floor(Math.random() * 10000);
  const newTask = {id, ...task}
  setTasks([...tasks, newTask])
}
const deleteTask = (id) => {
  setTasks(tasks.filter((task) => task.id !== id))
}

  return (
    <div className="container">
      <AddInput onAdd={addTask} />
      {tasks.length > 0 ? <Tasks tasks={tasks} onDelete={deleteTask}/>
      : <div className='empty-task'>No Tasks</div>}
    </div>

  );

  
}

export default App;